<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Alias entities.
 *
 * @ingroup postfix_admin
 */
interface AliasInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Alias name.
   *
   * @return string
   *   The Name of the Alias.
   */
  public function getAddress();

  /**
   * Sets the name.
   *
   * @param string $address
   *   The Name of the Alias.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setAddress($address = '');

  /**
   * Sets the forward addresses.
   *
   * @param array $goto
   *   The forward addresses.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setGoto(array $goto = []);

  /**
   * Gets the forward addresses.
   *
   * @return array
   *   Addersses.
   */
  public function getGoto();

  /**
   * Sets the forward addresses.
   *
   * @param string $domain
   *   The Domain Name.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setDomain($domain = []);

  /**
   * Gets the domain name.
   *
   * @return array
   *   The Domain Name.
   */
  public function getDomain();

  /**
   * Sets the status of forward addresses.
   *
   * @param bool $active
   *   The forward addresses is active or not.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setActive($active = TRUE);

  /**
   * Gets the status of forward addresses.
   *
   * @return array
   *   The forward addresses is active or not.
   */
  public function isActive();

  /**
   * Sets the status of forward addresses.
   *
   * @param bool $activated
   *   The forward addresses is activated or not.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setActivated($activated = FALSE);

  /**
   * Gets the status of forward addresses activation.
   *
   * @return array
   *   The forward addresses is activated or not.
   */
  public function isActivated();

  /**
   * Gets a token.
   *
   * @return string
   *   A Token.
   */
  public function getToken();

  /**
   * Sets a token.
   *
   * @param string $token
   *   A token to set.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setToken($token = '');

  /**
   * Create a token.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function createToken();

  /**
   * Create a token.
   *
   * @param string $to
   *   Send a confirmation email incl. an entity ID and a token to a user
   *   to validate an forwarding alias Email address.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function sendConfirmationEmail($to = '');

  /**
   * Gets the Alias creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Alias.
   */
  public function getCreatedTime();

  /**
   * Sets the Alias creation timestamp.
   *
   * @param int $timestamp
   *   The Alias creation timestamp.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Alias published status indicator.
   *
   * Unpublished Alias are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Alias is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Alias.
   *
   * @param bool $published
   *   TRUE to set this Alias to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\postfix_admin\Entity\AliasInterface
   *   The called Alias entity.
   */
  public function setPublished($published);

}
