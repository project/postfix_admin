<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Alias entities.
 */
class AliasViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    $data['address'] = [
      'field' => 'address',
      'title' => t('From'),
      'weight' => -10,
      'help' => t('The originate Email address'),
    ];

    $data['goto'] = [
      'field' => 'goto',
      'title' => t('To'),
      'weight' => -6,
      'help' => t('The Email address to forward'),
    ];

    $data['active'] = [
      'field' => 'active',
      'title' => t('Active'),
      'weight' => -5,
      'help' => t('The Alias Email address is active or not'),
    ];

    return $data;
  }

}
