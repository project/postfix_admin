<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Domain entities.
 *
 * @ingroup postfix_admin
 */
interface DomainInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Domain name.
   *
   * @return string
   *   Name of the Domain.
   */
  public function getDomain();

  /**
   * Sets the Domain name.
   *
   * @param string $domain
   *   The Domain name.
   *
   * @return \Drupal\postfix_admin\Entity\DomainInterface
   *   The called Domain entity.
   */
  public function setDomain($domain = '');

  /**
   * Gets the Description.
   *
   * @return string
   *   Name of the Description.
   */
  public function getDescription();

  /**
   * Sets the Description.
   *
   * @param string $description
   *   The Description.
   *
   * @return \Drupal\postfix_admin\Entity\DescriptionInterface
   *   The called Domain entity.
   */
  public function setDescription($description = '');

  /**
   * Gets the Aliases.
   *
   * @return string
   *   Name of the Aliases.
   */
  public function getAliases();

  /**
   * Sets the Aliases.
   *
   * @param string $aliases
   *   The Aliases.
   *
   * @return \Drupal\postfix_admin\Entity\AliasesInterface
   *   The called Domain entity.
   */
  public function setAliases($aliases = 0);

  /**
   * Gets the Mailboxes.
   *
   * @return string
   *   Name of the Mailboxes.
   */
  public function getMailboxes();

  /**
   * Sets the Mailboxes.
   *
   * @param string $mailboxes
   *   The Mailboxes.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxesInterface
   *   The called Domain entity.
   */
  public function setMailboxes($mailboxes = 0);

  /**
   * Gets the MaxQuota.
   *
   * @return string
   *   Name of the MaxQuota.
   */
  public function getMaxQuota();

  /**
   * Sets the MaxQuota.
   *
   * @param string $maxquota
   *   The MaxQuota.
   *
   * @return \Drupal\postfix_admin\Entity\MaxQuotaInterface
   *   The called Domain entity.
   */
  public function setMaxQuota($maxquota = 10);

  /**
   * Gets the Quota.
   *
   * @return string
   *   Name of the Quota.
   */
  public function getQuota();

  /**
   * Sets the Quota.
   *
   * @param string $quota
   *   The Quota.
   *
   * @return \Drupal\postfix_admin\Entity\QuotaInterface
   *   The called Domain entity.
   */
  public function setQuota($quota = 2048);

  /**
   * Gets the Transport.
   *
   * @return string
   *   Name of the Transport.
   */
  public function getTransport();

  /**
   * Sets the Transport.
   *
   * @param string $transport
   *   The Transport.
   *
   * @return \Drupal\postfix_admin\Entity\TransportInterface
   *   The called Domain entity.
   */
  public function setTransport($transport = '');

  /**
   * Gets the BackupMx.
   *
   * @return string
   *   Name of the BackupMx.
   */
  public function isBackupMx();

  /**
   * Sets the BackupMx.
   *
   * @param string $backupmx
   *   The BackupMx.
   *
   * @return \Drupal\postfix_admin\Entity\BackupMxInterface
   *   The called Domain entity.
   */
  public function setBackupMx($backupmx = FALSE);

  /**
   * Gets the status of active or not.
   *
   * @return bool
   *   Status of active.
   */
  public function isActive();

  /**
   * Sets the status of active or not.
   *
   * @param string $active
   *   The status of active.
   *
   * @return \Drupal\postfix_admin\Entity\BackupMxInterface
   *   The called Domain entity.
   */
  public function setActive($active = FALSE);

  /**
   * Gets the Domain creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Domain.
   */
  public function getCreatedTime();

  /**
   * Sets the Domain creation timestamp.
   *
   * @param int $timestamp
   *   The Domain creation timestamp.
   *
   * @return \Drupal\postfix_admin\Entity\DomainInterface
   *   The called Domain entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Domain changed timestamp.
   *
   * @return int
   *   Changed timestamp of the Domain.
   */
  public function getChangedTime();

  /**
   * Returns the Domain published status indicator.
   *
   * Unpublished Domain are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Domain is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Domain.
   *
   * @param bool $published
   *   TRUE to set this Domain to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\postfix_admin\Entity\DomainInterface
   *   The called Domain entity.
   */
  public function setPublished($published);

}
