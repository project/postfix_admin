<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Config entities.
 */
interface ConfigInterface extends ConfigEntityInterface {

  /**
   * Postfix Admin - Admin Username.
   */
  public function username();

  /**
   * Postfix Admin - Admin Password.
   */
  public function password();

  /**
   * Postfix Admin - Hostname.
   */
  public function hostname();

}
