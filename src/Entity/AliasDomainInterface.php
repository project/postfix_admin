<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Alias Domain entities.
 *
 * @ingroup postfix_admin
 */
interface AliasDomainInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Alias Domain name.
   *
   * @return string
   *   Name of the Alias Domain.
   */
  public function getAliasDomain();

  /**
   * Sets the Alias Domain name.
   *
   * @param string $alias_domain
   *   The Alias Domain name.
   *
   * @return \Drupal\postfix_admin\Entity\AliasDomainInterface
   *   The called Alias Domain entity.
   */
  public function setAliasDomain($alias_domain = '');

  /**
   * Gets the Target Domain name.
   *
   * @return string
   *   Name of the Target Domain.
   */
  public function getTargetDomain();

  /**
   * Sets the Target Domain name.
   *
   * @param string $target_domain
   *   The Target Domain name.
   *
   * @return \Drupal\postfix_admin\Entity\AliasDomainInterface
   *   The called Alias Domain entity.
   */
  public function setTargetDomain($target_domain = '');

  /**
   * Gets the status of active or not.
   *
   * @return bool
   *   Status of active.
   */
  public function isActive();

  /**
   * Sets the status of active or not.
   *
   * @param string $active
   *   The status of active.
   *
   * @return \Drupal\postfix_admin\Entity\BackupMxInterface
   *   The called Domain entity.
   */
  public function setActive($active = TRUE);

  /**
   * Gets the Alias Domain creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Alias Domain.
   */
  public function getCreatedTime();

  /**
   * Sets the Alias Domain creation timestamp.
   *
   * @param int $timestamp
   *   The Alias Domain creation timestamp.
   *
   * @return \Drupal\postfix_admin\Entity\AliasDomainInterface
   *   The called Alias Domain entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Alias Domain published status indicator.
   *
   * Unpublished Alias Domain are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Alias Domain is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Alias Domain.
   *
   * @param bool $published
   *   TRUE to set this Alias Domain to published,
   *   FALSE to set it to unpublished.
   *
   * @return \Drupal\postfix_admin\Entity\AliasDomainInterface
   *   The called Alias Domain entity.
   */
  public function setPublished($published);

}
