<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Domain entity.
 *
 * @ingroup postfix_admin
 *
 * @ContentEntityType(
 *   id = "domain",
 *   label = @Translation("Domain"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\postfix_admin\DomainListBuilder",
 *     "views_data" = "Drupal\postfix_admin\Entity\DomainViewsData",
 *     "translation" = "Drupal\postfix_admin\DomainTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\postfix_admin\Form\DomainForm",
 *       "add" = "Drupal\postfix_admin\Form\DomainForm",
 *       "edit" = "Drupal\postfix_admin\Form\DomainForm",
 *       "delete" = "Drupal\postfix_admin\Form\DomainDeleteForm",
 *     },
 *     "access" = "Drupal\postfix_admin\DomainAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\postfix_admin\DomainHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "domain",
 *   data_table = "domain_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer domain",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "domain",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/domain/{domain}",
 *     "add-form" = "/admin/structure/domain/add",
 *     "edit-form" = "/admin/structure/domain/{domain}/edit",
 *     "delete-form" = "/admin/structure/domain/{domain}/delete",
 *     "collection" = "/admin/structure/domain",
 *   },
 *   field_ui_base_route = "domain.settings"
 * )
 */
class Domain extends ContentEntityBase implements DomainInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain() {
    return $this->get('domain')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomain($domain = '') {
    $this->set('domain', $domain);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description = '') {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAliases() {
    return $this->get('aliases')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAliases($aliases = 0) {
    $this->set('aliases', $aliases);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailboxes() {
    return $this->get('mailboxes')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailboxes($mailboxes = 0) {
    $this->set('mailboxes', $mailboxes);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMaxQuota() {
    return $this->get('maxquota')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMaxQuota($maxquota = 10) {
    $this->set('maxquota', $maxquota);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuota() {
    return $this->get('quota')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuota($quota = 2048) {
    $this->set('quota', $quota);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTransport() {
    return $this->get('transport')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTransport($transport = '') {
    $this->set('transport', $transport);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isBackupMx() {
    return $this->get('backupmx')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setBackupMx($backupmx = FALSE) {
    $this->set('backupmx', $backupmx);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active = FALSE) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * Get domain list.
   *
   * @return string
   *   Return domains.
   */
  public static function getDomains() {

    // Check if the requested Email address already exists in mailbox or not.
    $entity_query = \Drupal::service('entity.query')->get('domain');
    $entity_query->condition('domain', 0, '<>');
    $query_result = $entity_query->execute();
    $domains = \Drupal::entityTypeManager()->getStorage('domain')->loadMultiple($query_result);
    $result = [];
    foreach ($domains as $domain) {
      $result[] = $domain->getDomain();
    }
    return $result ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Domain entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        // 'type' => 'entity_reference_autocomplete',.
        'type' => 'hidden',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The domain name for the entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['aliases'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Aliases'))
      ->setDescription(t('-1 = disable | 0 = unlimited.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['mailboxes'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Mailboxes'))
      ->setDescription(t('-1 = disable | 0 = unlimited.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['maxquota'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Max Quota'))
      ->setDescription(t('MB | -1 = disable | 0 = unlimited.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['quota'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quota'))
      ->setDescription(t('MB | -1 = disable | 0 = unlimited.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(0)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['transport'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Transport'))
      ->setDescription(t('Transport.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);

    $fields['backupmx'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Backup MX'))
      ->setDescription(t('Backup MX.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'checkbox',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'checkbox',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('The status of domain'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'checkbox',
        'weight' => 50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'checkbox',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Domain is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
