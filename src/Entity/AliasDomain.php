<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Alias Domain entity.
 *
 * @ingroup postfix_admin
 *
 * @ContentEntityType(
 *   id = "alias_domain",
 *   label = @Translation("Alias Domain"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\postfix_admin\AliasDomainListBuilder",
 *     "views_data" = "Drupal\postfix_admin\Entity\AliasDomainViewsData",
 *     "translation" = "Drupal\postfix_admin\AliasDomainTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\postfix_admin\Form\AliasDomainForm",
 *       "add" = "Drupal\postfix_admin\Form\AliasDomainForm",
 *       "edit" = "Drupal\postfix_admin\Form\AliasDomainForm",
 *       "delete" = "Drupal\postfix_admin\Form\AliasDomainDeleteForm",
 *     },
 *     "access" = "Drupal\postfix_admin\AliasDomainAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\postfix_admin\AliasDomainHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "alias_domain",
 *   data_table = "alias_domain_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer alias domain",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "alias_domain",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alias_domain/{alias_domain}",
 *     "add-form" = "/admin/structure/alias_domain/add",
 *     "edit-form" = "/admin/structure/alias_domain/{alias_domain}/edit",
 *     "delete-form" = "/admin/structure/alias_domain/{alias_domain}/delete",
 *     "collection" = "/admin/structure/alias_domain",
 *   },
 *   field_ui_base_route = "alias_domain.settings"
 * )
 */
class AliasDomain extends ContentEntityBase implements AliasDomainInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAliasDomain() {
    return $this->get('alias_domain')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAliasDomain($alias_domain = '') {
    $this->set('alias_domain', $alias_domain);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetDomain() {
    return $this->get('target_domain')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetDomain($target_domain = '') {
    $this->set('target_domain', $target_domain);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active = FALSE) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Alias Domain entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        // 'type' => 'entity_reference_autocomplete',.
        'type' => 'hidden',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['alias_domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Alias Domain'))
      ->setDescription(t('The alias domain name for the entity.'))
      /*
       * ->setSettings([
       * 'max_length' => 50,
       * 'text_processing' => 0,
       * ])
       * ->setDefaultValue('')
       * ->setDisplayOptions('view', [
       * 'label' => 'above',
       * 'type' => 'string',
       * 'weight' => -4,
       * ])
       * ->setDisplayConfigurable('form', TRUE)
       * ->setDisplayConfigurable('view', TRUE)
       */
      ->setRequired(TRUE);

    $fields['target_domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Target Domain'))
      ->setDescription(t('The target domain name for the entity.'))
      ->setDefaultValue('')

      /*
       * ->setSettings([
       * 'max_length' => 50,
       * 'text_processing' => 0,
       * ])
       * ->setDefaultValue('')
       * ->setDisplayOptions('view', [
       * 'label' => 'above',
       * 'type' => 'string',
       * 'weight' => -4,
       * ])
       * ->setDisplayConfigurable('form', TRUE)
       * ->setDisplayConfigurable('view', TRUE)
       */
      ->setRequired(TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the Alias is active.'))
      ->setDefaultValue(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Alias Domain is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        // 'type' => 'checkbox',.
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
