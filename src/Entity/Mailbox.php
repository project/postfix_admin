<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Mailbox entity.
 *
 * @ingroup postfix_admin
 *
 * @ContentEntityType(
 *   id = "mailbox",
 *   label = @Translation("Mailbox"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\postfix_admin\MailboxListBuilder",
 *     "views_data" = "Drupal\postfix_admin\Entity\MailboxViewsData",
 *     "translation" = "Drupal\postfix_admin\MailboxTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\postfix_admin\Form\MailboxForm",
 *       "add" = "Drupal\postfix_admin\Form\MailboxForm",
 *       "edit" = "Drupal\postfix_admin\Form\MailboxForm",
 *       "delete" = "Drupal\postfix_admin\Form\MailboxDeleteForm",
 *     },
 *     "access" = "Drupal\postfix_admin\MailboxAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\postfix_admin\MailboxHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "mailbox",
 *   data_table = "mailbox_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer mailbox",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "username",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/mailbox/{mailbox}",
 *     "add-form" = "/admin/structure/mailbox/add",
 *     "edit-form" = "/admin/structure/mailbox/{mailbox}/edit",
 *     "delete-form" = "/admin/structure/mailbox/{mailbox}/delete",
 *     "collection" = "/admin/structure/mailbox",
 *   },
 *   field_ui_base_route = "mailbox.settings"
 * )
 */
class Mailbox extends ContentEntityBase implements MailboxInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getUsername() {
    return $this->get('username')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsername($username = '') {
    $this->set('username', $username);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPassword() {
    return $this->get('password')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPassword($password = '') {
    $this->set('password', $password);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPasswordConfirmation() {
    return $this->get('password_confirmation')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPasswordConfirmation($password_confirmation = '') {
    $this->set('password_confirmation', $password_confirmation);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name = '') {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailDir() {
    return $this->get('maildir')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMailDir($maildir = '') {
    $this->set('maildir', $maildir);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuota() {
    return $this->get('quota')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuota($quota = 10) {
    $this->set('quota', $quota);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalPart() {
    return $this->get('local_part')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocalPart($local_part = '') {
    $this->set('local_part', $local_part);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain() {
    return $this->get('domain')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDomain($domain = '') {
    $this->set('domain', $domain);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active = FALSE) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhone() {
    return $this->get('phone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhone($phone = '') {
    $this->set('phone', $phone);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmailOther() {
    return $this->get('email_other')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmailOther($email_other = '') {
    $this->set('email_other', $email_other);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    if ($this->get('token_expiration')->value < (new \DateTime())->getTimeStamp()) {
      return $this->get('token')->value;
    }
    else {
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setToken($token = '') {
    $this->set('token', $token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createToken() {

    $uuid = \Drupal::service('uuid')->generate();
    $this->set('token_expiration', (new \DateTime())->getTimestamp() + $this->TOKEN_EXPIRATION);
    $this->set('token', $uuid);
    return $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Username'))
      ->setDescription(t('The username of the Mailbox.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -5,
        'settings' => [
          'size' => '60',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['username'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Local Part'))
      ->setDescription(t('The Email address.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDefaultValue('')
      ->setReadOnly(TRUE)
      ->setRequired(TRUE);

    $fields['local_part'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Local Part'))
      ->setDescription(t('The Local Part of the Email address.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -50,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -50,
      ])
      ->setDefaultValue('')
      ->setReadOnly(TRUE)
      ->setRequired(TRUE);

    $fields['domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain'))
      ->setDescription(t('The domain name of the Email address.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'options_select',
        'weight' => -48,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -48,
      ])
      ->setDefaultValue('')
      ->setRequired(TRUE);

    $fields['password'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Password'))
      ->setDescription(t('The password for POP3/IMAP.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'password',
        'weight' => -46,
      ])
      ->setDisplayOptions('form', [
        'type' => 'password',
        'weight' => -46,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['password_confirmation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Confirm Password'))
      ->setDescription(t('The password confirmation for POP3/IMAP.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'password',
        'weight' => -46,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The full name of the Mailbox user.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -44,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -44,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['maildir'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Mail Dir'))
      ->setDescription(t('The mail_dir of the Mailbox.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setRequired(FALSE);

    $fields['quota'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Quota'))
      ->setDescription(t('MB (max: 10).'))
      ->setSettings([
        'max_length' => 10,
        'text_processing' => 0,
      ])
      ->setDefaultValue(10)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setRequired(TRUE);

    $fields['phone'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Phone'))
      ->setDescription(t('The phone number of the Mailbox user.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['email_other'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Email Other'))
      ->setDescription(t('The other Email address of the Mailbox user.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['token_expiration'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Token Expiration'))
      ->setDescription(t('The time when a token is created.'));

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('A string token to confirm the alias is valid email address.'))
      ->setDefaultValue(TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the Alias is active.'))
      ->setDefaultValue(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Mailbox is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
