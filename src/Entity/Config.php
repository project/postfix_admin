<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Config entity.
 *
 * @ConfigEntityType(
 *   id = "config",
 *   label = @Translation("Config"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\postfix_admin\ConfigListBuilder",
 *     "form" = {
 *       "add"    = "Drupal\postfix_admin\Form\ConfigForm",
 *       "edit"   = "Drupal\postfix_admin\Form\ConfigForm",
 *       "delete" = "Drupal\postfix_admin\Form\ConfigDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\postfix_admin\ConfigHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "config",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical"   = "/admin/config/services/postfix_admin/{config}",
 *     "add-form"    = "/admin/config/services/postfix_admin/add",
 *     "edit-form"   = "/admin/config/services/postfix_admin/{config}/edit",
 *     "delete-form" = "/admin/config/services/postfix_admin/{config}/delete",
 *     "collection"  = "/admin/config/services/postfix_admin"
 *   }
 * )
 */
class Config extends ConfigEntityBase implements ConfigInterface {

  /**
   * The Config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Config label.
   *
   * @var string
   */
  protected $label;

  /**
   * Postfix Admin - Admin Username.
   *
   * @var string
   */
  protected $username;

  /**
   * Postfix Admin - Admin  Password.
   *
   * @var string
   */
  protected $password;

  /**
   * Postfix Admin Hostname.
   *
   * @var string
   */
  protected $hostname;

  /**
   * Postfix Admin Username.
   */
  public function username() {

    return $this->username;
  }

  /**
   * Postfix Admin - Admin  Password.
   */
  public function password() {

    return $this->password;
  }

  /**
   * Postfix Admin Hostname.
   */
  public function hostname() {

    return $this->hostname;
  }

}
