<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Domain entities.
 */
class DomainViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
