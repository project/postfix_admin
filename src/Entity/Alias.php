<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines the Alias entity.
 *
 * @ingroup postfix_admin
 *
 * @ContentEntityType(
 *   id = "alias",
 *   label = @Translation("Alias"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\postfix_admin\AliasListBuilder",
 *     "views_data" = "Drupal\postfix_admin\Entity\AliasViewsData",
 *     "translation" = "Drupal\postfix_admin\AliasTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\postfix_admin\Form\AliasForm",
 *       "add" = "Drupal\postfix_admin\Form\AliasForm",
 *       "edit" = "Drupal\postfix_admin\Form\AliasForm",
 *       "delete" = "Drupal\postfix_admin\Form\AliasDeleteForm",
 *     },
 *     "access" = "Drupal\postfix_admin\AliasAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\postfix_admin\AliasHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "alias",
 *   data_table = "alias_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer alias",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "address",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/alias/{alias}",
 *     "add-form" = "/admin/structure/alias/add",
 *     "edit-form" = "/admin/structure/alias/{alias}/edit",
 *     "delete-form" = "/admin/structure/alias/{alias}/delete",
 *     "collection" = "/admin/structure/alias",
 *   },
 *   field_ui_base_route = "alias.settings"
 * )
 */
class Alias extends ContentEntityBase implements AliasInterface {

  use EntityChangedTrait;

  // @TODO: HARD CORDED
  public const TOKEN_EXPIRATION = 60 * 60 * 24;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->get('address')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAddress($address = '') {
    $this->set('address', $address);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    $this->get('created')->value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGoto() {
    return [$this->get('goto')->value];
  }

  /**
   * {@inheritdoc}
   */
  public function setGoto($goto = []) {
    $this->set('goto', $goto);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDomain() {
    return [$this->get('domain')->value];
  }

  /**
   * {@inheritdoc}
   */
  public function setDomain($domain = []) {
    $this->set('domain', $domain);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active = TRUE) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActivated() {
    return $this->get('activated')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActivated($activated = FALSE) {
    $this->set('activated', $activated);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    if ($this->get('token_expiration')->value < (new \DateTime())->getTimeStamp()) {
      return $this->get('token')->value;
    }
    else {
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setToken($token = '') {
    $this->set('token', $token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function createToken() {

    $uuid = \Drupal::service('uuid')->generate();
    $this->set('token_expiration', (new \DateTime())->getTimestamp() + $this->TOKEN_EXPIRATION);
    $this->set('token', $uuid);
    return $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public function sendConfirmationEmail($goto = []) {

    $result = [
      'message' => t('There was a problem sending your message and it was not sent.'),
      'status' => 'error',
      'value' => FALSE,
    ];

    if ($this->id() !== NULL) {

      $module = 'postfix_admin';
      $key = 'validate_email';

      $langcode = \Drupal::currentUser()->getPreferredLangcode();
      $options = [
        'langcode' => $langcode,
      ];

      $message = t("You have requested to verify an alias email address: @address.\n\nBefore you can forward from @to to your alias email address @address, please click the link below to confirm your\nrequest:\n\n@url\n\nIf you click the link and it appears to be broken, please copy and paste it into a new browser window.\n\nThanks for using Postfix Admin!",
        [
          '@to' => $goto[0],
          '@address' => $this->label(),
          '@url' => sprintf('https://%s/api/postfix_admin/confirm?id=%s&token=%s',
            \Drupal::request()->getHost(),
            $this->id(),
            $this->createToken(),
          ),
        ]
      );

      $params['message'] = sprintf('https://%s/api/postfix_admin/confirm?id=%s&token=%s',
         \Drupal::request()->getHost(),
        $this->id(),
        $this->get('token')->value
      );
      $params['message'] = $message;

      $params['from'] = \Drupal::config('system.site')->get('mail');
      // @TOOD: Consider multiple
      $params['subject'] = t('Confirmation Email for @address', ['@address' => $this->label()], $options);
      $send = TRUE;

      foreach ($goto as $to) {
        $result = \Drupal::service('plugin.manager.mail')->mail($module, $key, $to, $langcode, $params, NULL, $send);
        if ($result['result'] === TRUE) {
          $result = [
            'message' => t('Your message has been sent to: %to.', ['%to' => $to]),
            'status' => 'status',
            'value' => TRUE,
          ];
        }
      }
    }

    \Drupal::service('messenger')->addMessage($result['message'], $result['status']);
    return $result['value'];

  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Alias entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        // 'type' => 'entity_reference_autocomplete',.
        'type' => 'hidden',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Address'))
      ->setDescription(t('The Alias Email Address.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Don't add ->setDisplayOptions(...)
    $fields['domain'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Domain'))
      ->setDescription(t('The domain name of the Alias Email address.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    // Don't add ->setDisplayOptions(...)
    $fields['goto'] = BaseFieldDefinition::create('string')
      ->setLabel(t('To'))
      ->setDescription(t('Accepts multiple targets, one entry per line.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('A boolean indicating whether the Alias is active.'))
      ->setDefaultValue(FALSE);

    $fields['activated'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Activated'))
      ->setDescription(t('A boolean indicating whether the Alias is activated.'))
      ->setDefaultValue(FALSE);

    $fields['token_expiration'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Token Expiration'))
      ->setDescription(t('The time when a token is created.'));

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('A string token to confirm the alias is valid email address.'))
      ->setDefaultValue(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Alias is published.'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        // 'type' => 'checkbox',.
        'weight' => 3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
