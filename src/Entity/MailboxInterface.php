<?php

namespace Drupal\postfix_admin\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Mailbox entities.
 *
 * @ingroup postfix_admin
 */
interface MailboxInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Username.
   *
   * @return string
   *   Name of the Username.
   */
  public function getUsername();

  /**
   * Gets the Password.
   *
   * @return string
   *   Name of the Password.
   */
  public function getPassword();

  /**
   * Sets the Password.
   *
   * @param string $password
   *   The Uername.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setPassword($password);

  /**
   * Gets the Password.
   *
   * @return string
   *   Name of the Password.
   */
  public function getPasswordConfirmation();

  /**
   * Sets the Password.
   *
   * @param string $password_confirmation
   *   Name of the password.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setPasswordConfirmation($password_confirmation = '');

  /**
   * Gets the Mailbox name.
   *
   * @return string
   *   Name of the Mailbox.
   */
  public function getName();

  /**
   * Sets the Mailbox name.
   *
   * @param string $name
   *   The Mailbox name.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setName($name);

  /**
   * Gets the Mailbox maildir.
   *
   * @return string
   *   MailDir of the Mailbox.
   */
  public function getMailDir();

  /**
   * Sets the Mailbox maildir.
   *
   * @param string $maildir
   *   The Mailbox maildir.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setMailDir($maildir);

  /**
   * Gets the Mailbox quota.
   *
   * @return string
   *   Quota of the Mailbox.
   */
  public function getQuota();

  /**
   * Sets the Mailbox quota.
   *
   * @param string $quota
   *   The Mailbox quota.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setQuota($quota);

  /**
   * Gets the Mailbox local_part.
   *
   * @return string
   *   LocalPart of the Mailbox.
   */
  public function getLocalPart();

  /**
   * Sets the Mailbox local_part.
   *
   * @param string $local_part
   *   The Mailbox local_part.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setLocalPart($local_part);

  /**
   * Gets the Mailbox domain.
   *
   * @return string
   *   Domain of the Mailbox.
   */
  public function getDomain();

  /**
   * Sets the Mailbox domain.
   *
   * @param string $domain
   *   The Mailbox domain.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setDomain($domain);

  /**
   * Gets the Mailbox active.
   *
   * @return string
   *   Name of the Mailbox.
   */
  public function isActive();

  /**
   * Sets the Mailbox active.
   *
   * @param bool $active
   *   The Mailbox active.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setActive($active = FALSE);

  /**
   * Gets the Mailbox phone.
   *
   * @return string
   *   Phone of the Mailbox.
   */
  public function getPhone();

  /**
   * Sets the Mailbox phone.
   *
   * @param string $phone
   *   The Mailbox phone.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setPhone($phone);

  /**
   * Gets the Mailbox email_other.
   *
   * @return string
   *   EmailOther of the Mailbox.
   */
  public function getEmailOther();

  /**
   * Sets the Mailbox email_other.
   *
   * @param string $email_other
   *   The Mailbox email_other.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setEmailOther($email_other);

  /**
   * Gets the Mailbox token.
   *
   * @return string
   *   Token of the Mailbox.
   */
  public function getToken();

  /**
   * Sets the Mailbox token.
   *
   * @param string $token
   *   The Mailbox token.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setToken($token);

  /**
   * Create the Mailbox token.
   *
   * @return string
   *   Created token.
   */
  public function createToken();

  /**
   * Gets the Mailbox creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Mailbox.
   */
  public function getCreatedTime();

  /**
   * Sets the Mailbox creation timestamp.
   *
   * @param int $timestamp
   *   The Mailbox creation timestamp.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Mailbox published status indicator.
   *
   * Unpublished Mailbox are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Mailbox is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Mailbox.
   *
   * @param bool $published
   *   TRUE to set this Mailbox to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\postfix_admin\Entity\MailboxInterface
   *   The called Mailbox entity.
   */
  public function setPublished($published);

}
