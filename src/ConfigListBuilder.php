<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Config entities.
 */
class ConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('username', 0, '<>');
    $entity_query->pager(50);
    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $configs = $entity_query->execute();
    return $this->storage->loadMultiple($configs);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'hostame' => [
        'data' => $this->t('Hostname'),
        'field' => 'hostname',
        'specifier' => 'hostname',
      ],
      'username' => [
        'data' => $this->t('Username'),
        'field' => 'username',
        'specifier' => 'username',
      ],
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    // You probably want a few more properties here...
    $row['hostname'] = $entity->hostname();
    $row['username'] = $entity->username();

    return $row + parent::buildRow($entity);
  }

}
