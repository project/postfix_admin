<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Alias entities.
 *
 * @ingroup postfix_admin
 */
class AliasListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('address', 0, '<>');
    $entity_query->pager(100);
    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $addresses = $entity_query->execute();
    return $this->storage->loadMultiple($addresses);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'address' => [
        'data' => $this->t('From'),
        'field' => 'address',
        'specifier' => 'address',
      ],
      'goto' => [
        'data' => $this->t('To'),
        'field' => 'goto',
        'specifier' => 'goto',
      ],
      'active' => [
        'data' => $this->t('Active'),
        'field' => 'active',
        'specifier' => 'active',
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\postfix_admin\Entity\Alias */
    $row['address'] = $entity->getAddress();
    //
    // $row['address'] = Link::createFromRoute(
    // $entity->label(),
    // 'entity.alias.edit_form',
    // ['address' => $entity->id()]
    // );
    $row['goto'] = implode(', ', $entity->getGoto());
    // @IMPORTANT: add ['data'] here
    $row['active']['data'] = [
      '#type' => 'checkbox',
      '#default_value' => (bool) $entity->isActive(),
      '#checked' => (bool) $entity->isActive(),
      '#attributes' => ['disabled' => 'disabled'],
      '#disabled' => TRUE,
    ];

    return $row + parent::buildRow($entity);
  }

}
