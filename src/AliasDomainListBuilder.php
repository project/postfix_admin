<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Alias Domain entities.
 *
 * @ingroup postfix_admin
 */
class AliasDomainListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('alias_domain', 0, '<>');
    $entity_query->pager(50);
    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $alias_domains = $entity_query->execute();
    return $this->storage->loadMultiple($alias_domains);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'alias_domain' => [
        'data' => $this->t('Alias Domain'),
        'field' => 'alias_domain',
        'specifier' => 'alias_domain',
      ],
      'target_domain' => [
        'data' => $this->t('Target Domain'),
        'field' => 'target_domain',
        'specifier' => 'target_domain',
      ],
      'active' => [
        'data' => $this->t('Active'),
        'field' => 'active',
        'specifier' => 'active',
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\postfix_admin\Entity\Alias */
    $row['alias_domain'] = Link::createFromRoute(
      $entity->label(),
      'entity.alias_domain.edit_form',
      ['alias_domain' => $entity->id()]
    );
    $row['target_domain'] = $entity->getTargetDomain();
    // @IMPORTANT: add ['data'] here
    $row['active']['data'] = [
      '#type' => 'checkbox',
      '#default_value' => (bool) $entity->isActive(),
      '#checked' => (bool) $entity->isActive(),
      '#attributes' => ['disabled' => 'disabled'],
      '#disabled' => TRUE,
    ];

    return $row + parent::buildRow($entity);
  }

}
