<?php

namespace Drupal\postfix_admin;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for alias_domain.
 */
class AliasDomainTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
