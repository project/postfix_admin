<?php

namespace Drupal\postfix_admin\Controller;

require_once 'Zend/XmlRpc/Client.php';

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Query\QueryFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\postfix_admin\Entity\Alias;
use Drupal\postfix_admin\Entity\Config;

/**
 * Class PostfixAdminController.
 */
class PostfixAdminController extends ControllerBase implements PostfixAdminControllerInterface {

  /**
   * For Postfix Admin at https://sourceforge.net/projects/postfix_admin/ .
   */

  /**
   * An aliasProxy variable.
   *
   * @var string
   */
  protected $aliasProxy  = NULL;
  protected $userProxy   = NULL;
  protected $domainProxy = NULL;
  protected $xmlrpc      = NULL;

  /**
   * Hostname: 'https://example.com/postfix_admin/xmlrpc.php'; .
   *
   * @var string
   */
  protected $hostname = NULL;

  /**
   * A Costructor.
   */
  public function __construct(QueryFactory $query_factory) {

    // Use the factory to create a query object for node.
    $entity_query = \Drupal::service('entity.query')->get('config');
    // Add a filter (published).
    $entity_query->condition('status', 1);
    // Run the query.
    $configs = $entity_query->execute();
    // @TODO Hard Corded
    $config = Config::load(array_values($configs)[0]);
    if (isset($config)) {
      $this->username = $config->username();
      $this->password = $config->password();
      $this->hostname = $config->hostname() . '/xmlrpc.php';
    }

    $this->xmlrpc = new \Zend_XmlRpc_Client($this->hostname);
    $http_client = $this->xmlrpc->getHttpClient();
    $http_client->setCookieJar();

    $this->postfix_admin = $this->xmlrpc->getProxy('login');
    $success = $this->postfix_admin->login($this->username, $this->password);

    if (!$success) {
      return NULL;
    }

    if (empty($this->userProxy)) {
      $this->userProxy = $this->xmlrpc->getProxy('user');
    }

    if (empty($this->aliasProxy)) {
      $this->aliasProxy = $this->xmlrpc->getProxy('alias');
    }

    if (empty($this->domainProxy)) {
      $this->domainProxy = $this->xmlrpc->getProxy('domain');
    }
  }

  /**
   * Create $container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // User the $container to get a query factory object.
      // This object lets us create query objects.
      $container->get('entity.query')
    );
  }

  /**
   * Change Password.
   *
   * @return string
   *   Return Hello string.
   */
  public function changePassword($old_password = '', $new_password = '') {
    $result = $this->userProxy->changePassword($old_password, $new_password);

    return [
      '#type' => 'markup',
      '#markup' => 'result:<br />--------------------<br />' . implode('<br />', $result) . '<br />--------------------',
    ];
  }

  /**
   * Create Alias.
   *
   * @return bool
   *   Return boolean true.
   */
  public function set($alias = '', $goto = [], $flags = '') {
    $result = $this->aliasProxy->set($alias, $goto, $flags);
    return $result;
  }

  /**
   * Get.
   *
   * @return string
   *   Return Hello string.
   */
  public function get() {

    $result = $this->aliasProxy->get();
    return [
      '#type' => 'markup',
      '#markup' => 'result:<br />--------------------<br />' . implode('<br />', $result) . '<br />--------------------',
    ];
  }

  /**
   * Delete Alias.
   *
   * @return bool
   *   Return flag.
   */
  public function delete($alias = '') {

    $result = $this->aliasProxy->delete($alias);
    return $result;
    /*
    return [
    '#type' => 'markup',
    '#markup' => 'result: ' . ($result ? 'true' : 'false'),
    ];
     */
  }

  /**
   * Has Store And Forward.
   *
   * @return string
   *   Return Hello string.
   */
  public function hasStoreAndForward() {

    $result = $this->aliasProxy->hasStoreAndForward();

    return [
      '#type' => 'markup',
      '#markup' => 'result: ' . ($result ? 'true' : 'false'),
    ];
  }

  /**
   * Get domain list.
   *
   * @return string
   *   Return domains.
   */
  public function getDomains() {

    // Check if the requested Email address already exists in mailbox or not.
    $entity_query = $this->query_factory->get('domain');
    $entity_query->condition('domain', 0, '<>');
    $query_result = $entity_query->execute();
    $domains = \Drupal::entityTypeManager()->getStorage('domain')->loadMultiple($query_result);
    $result = [];
    foreach ($domains as $domain) {
      $result[] = $domain->getDomain();
    }
    return $result ?: [];
  }

  /**
   * Confirm Token.  If the token is valid, the token will be deleted.
   *
   * @return bool
   *   Return the flag.
   */
  public function confirmToken(Request $request = NULL) {

    $id = $request->get('id');
    $token = $request->get('token');

    $entity = Alias::load($id);
    $address = $entity->getAddress();
    if (isset($entity)
    && $entity->getToken() === $token) {

      $entity->setActive(TRUE);
      $entity->setActivated(TRUE);
      $entity->setToken('');
      $entity->save();

      $message = t('Confirmed Alias Email address: "@goto". Check at @url.', [
      // @TODO: Handle multiple Email addresses
        '@goto' => $entity->getGoto()[0],
        '@url' => $entity->toLink($entity->getAddress(), 'edit-form')->toString(),
      ]);

      return [
        '#type' => 'markup',
      // @TODO: Handle multiple email addresses.
        '#markup' => $message,
      ];

    }
    else {

      $entity->setActivated(FALSE);
      $entity->setActive(FALSE);
      $entity->save();

      return [
        '#type' => 'markup',
        '#markup' => "Confirm Failed:<br />Entity ID: $id<br />Your Token: $token",
      ];
    }
  }

}
