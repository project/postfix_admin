<?php

namespace Drupal\postfix_admin\Controller;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class PostfixAdminControllerInterface.
 */
interface PostfixAdminControllerInterface {

  /**
   * Change Password.
   *
   * @return string
   *   Return flag.
   */
  public function changePassword($old_password = '', $new_password = '');

  /**
   * Get.
   *
   * @return string
   *   Return alias addresses.
   */
  public function get();

  /**
   * Create if $flag='', update if $flag='remote_only' or 'forward_and_store'.
   */
  public function set($address = '', $goto = [], $flags = '');

  /**
   * Delete.
   *
   * @return string
   *   Return boolean.
   */
  public function delete($address = '');

  /**
   * Has Store And Forward.
   *
   * @return string
   *   Return flag.
   */
  public function hasStoreAndForward();

  /**
   * Domain list.
   *
   * @return string
   *   Return Domain list.
   */
  public function getDomains();

  /**
   * Confirm Token.  If the token is valid, the token will be deleted.
   *
   * @return bool
   *   Return the flag.
   */
  public function confirmToken(Request $request = NULL);

}
