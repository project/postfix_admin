<?php

namespace Drupal\postfix_admin;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for alias.
 */
class AliasTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
