<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Mailbox entities.
 *
 * @ingroup postfix_admin
 */
class MailboxListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('username', 0, '<>');
    $entity_query->pager(50);
    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $usernames = $entity_query->execute();
    return $this->storage->loadMultiple($usernames);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'username' => [
        'data' => $this->t('Email Address'),
        'field' => 'username',
        'specifier' => 'username',
      ],
      'domain' => [
        'data' => $this->t('To'),
        'field' => 'domain',
        'specifier' => 'domain',
      ],
      'name' => [
        'data' => $this->t('Name'),
        'field' => 'name',
        'specifier' => 'name',
      ],
      'quota' => [
        'data' => $this->t('Quota (MB)'),
        'field' => 'quota',
        'specifier' => 'quota',
      ],
      'active' => [
        'data' => $this->t('Active'),
        'field' => 'active',
        'specifier' => 'active',
      ],
      'changed' => [
        'data' => $this->t('Last Modified'),
        'field' => 'changed',
        'specifier' => 'changed',
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\postfix_admin\Entity\Mailbox */
    $row['username'] = Link::createFromRoute(
      $entity->label(),
      'entity.mailbox.edit_form',
      ['mailbox' => $entity->id()]
    );

    // @TODO HARD CODED.
    $row['domain'] = 'Mailbox';
    $row['name'] = $entity->getName();
    $row['quota'] = (int) $entity->getQuota() / 1024 / 1000;
    // @IMPORTANT: add ['data'] here.
    $row['active']['data'] = [
      '#type' => 'checkbox',
      '#default_value' => (bool) $entity->isActive(),
      '#checked' => (bool) $entity->isActive(),
      '#attributes' => ['disabled' => 'disabled'],
      '#disabled' => TRUE,
    ];
    $row['changed'] = \Drupal::service('date.formatter')->format($entity->getChangedTime(), 'short');

    return $row + parent::buildRow($entity);
  }

}
