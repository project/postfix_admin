<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Alias Domain entity.
 *
 * @see \Drupal\postfix_admin\Entity\AliasDomain.
 */
class AliasDomainAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\postfix_admin\Entity\AliasDomainInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished alias domain');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published alias domain');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit alias domain');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete alias domain');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add alias domain');
  }

}
