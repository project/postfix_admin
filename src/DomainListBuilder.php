<?php

namespace Drupal\postfix_admin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Domain entities.
 *
 * @ingroup postfix_admin
 */
class DomainListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->storage->getQuery();
    $entity_query->condition('domain', 0, '<>');
    $entity_query->pager(50);
    $header = $this->buildHeader();
    $entity_query->tableSort($header);
    $domains = $entity_query->execute();
    return $this->storage->loadMultiple($domains);
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'domain' => [
        'data' => $this->t('Domain'),
        'field' => 'domain',
        'specifier' => 'domain',
      ],
      'description' => [
        'data' => $this->t('Description'),
        'field' => 'description',
        'specifier' => 'description',
      ],
      'aliases' => [
        'data' => $this->t('Aliases'),
        'field' => 'aliases',
        'specifier' => 'aliases',
      ],
      'mailboxes' => [
        'data' => $this->t('Mailboxes'),
        'field' => 'mailboxes',
        'specifier' => 'mailboxes',
      ],
      'maxquota' => [
        'data' => $this->t('Max Quota (MB)'),
        'field' => 'maxquota',
        'specifier' => 'maxquota',
      ],
      'quota' => [
        'data' => $this->t('Domain Quota'),
        'field' => 'quota',
        'specifier' => 'quota',
      ],
      'backupmx' => [
        'data' => $this->t('Mail Server is backup MX'),
        'field' => 'backupmx',
        'specifier' => 'backupmx',
      ],
      'active' => [
        'data' => $this->t('Active'),
        'field' => 'active',
        'specifier' => 'active',
      ],
      'changed' => [
        'data' => $this->t('Last Modified'),
        'field' => 'changed',
        'specifier' => 'changed',
      ],
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\postfix_admin\Entity\Domain */
    $row['domain'] = Link::createFromRoute(
      $entity->label(),
      'entity.domain.edit_form',
      ['domain' => $entity->id()]
    );
    $row['description'] = $entity->getDescription();
    $row['aliases'] = $entity->getAliases();
    $row['mailboxes'] = $entity->getMailboxes();
    $row['maxquota'] = $entity->getMaxQuota();
    $row['quota'] = $entity->getQuota();
    // @IMPORTANT: add ['data'] here.
    $row['backupmx']['data'] = [
      '#type' => 'checkbox',
      '#default_value' => (bool) $entity->isBackupMx(),
      '#checked' => (bool) $entity->isBackupMx(),
      '#attributes' => ['disabled' => 'disabled'],
      '#disabled' => TRUE,
    ];
    // @IMPORTANT: add ['data'] here.
    $row['active']['data'] = [
      '#type' => 'checkbox',
      '#default_value' => (bool) $entity->isActive(),
      '#checked' => (bool) $entity->isActive(),
      '#attributes' => ['disabled' => 'disabled'],
      '#disabled' => TRUE,
    ];
    $row['changed'] = \Drupal::service('date.formatter')->format($entity->getChangedTime(), 'short');
    return $row + parent::buildRow($entity);
  }

}
