<?php

namespace Drupal\postfix_admin\Form;

use Drupal\user\Entity\User;
use Drupal\postfix_admin\Entity\Domain;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form controller for Alias edit forms.
 *
 * @ingroup postfix_admin
 */
class AliasForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\postfix_admin\Entity\Alias */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    // The account name before '@' in email address.
    $account = User::load(\Drupal::currentUser()->id());
    $local_part = $account->getUsername();
    if (!$entity->isNew()
    && \Drupal::currentUser()->hasPermission('administer postfix_admin')) {
      $local_part = explode('@', $entity->getAddress())[0];
    }

    $form['localpart'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Name'),
      '#maxlength' => 255,
      '#default_value' => $local_part,
      '#description' => \Drupal::currentUser()->hasPermission('administer postfix_admin')
      ? $this->t('To create a catch-all use an "*" as alias.')
      : '',
      '#weight' => -10,
      '#disabled' => !\Drupal::currentUser()->hasPermission('administer postfix_admin'),
      '#required' => TRUE,
    ];

    // The domain name.
    $selected_domain = explode('@', $entity->getAddress())[1];
    $options = [];
    foreach (Domain::getDomains() as $domain) {
      $options[$domain] = $domain;
    }

    if (empty($options)) {
      \Drupal::service('messenger')->addMessage($this->t('Please add domain names at first.'), 'warning', FALSE);
      $response = new RedirectResponse(\Drupal::url('entity.domain.add_form'));
      $response->send();
    }

    asort($options);
    $form['domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Domain'),
      '#options' => $options,
      '#default_value' => $selected_domain,
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('Select a domain name'),
      '#weight' => -8,
      '#required' => TRUE,
    ];

    // The forward Addresses.
    $form['goto'] = [
      '#type' => 'textfield',
      '#title' => $this->t('To'),
      '#cols' => 50,
      '#default_value' => $entity->getGoto()[0],
      '#description' => $this->t('Enter a valid email address to forward.'),
      '#weight' => -5,
      '#required' => TRUE,
    ];

    // Is Activated?.
    if ($entity->isActivated()
    || \Drupal::currentUser()->hasPermission('administer postfix_admin')) {

      $form['active'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Active'),
        '#default_value' => $entity->isNew()
        || ($entity->isActive() && empty($entity->getToken())),
        '#description' => $this->t('Check to enable the alias / forwarder function.'),
        '#attributes'  => ['readonly' => 'readonly'],
        '#weight' => 50,
      ];
    }
    else {

      $form['active'] = [
        '#type' => 'hidden',
        '#default_value' => FALSE,
        '#attributes'  => ['readonly' => 'readonly'],
        '#weight' => 50,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entity;
    $gotoes = explode(',', trim($form_state->getValue('goto')));
    // @TODO: Handle multiple email addresses.
    foreach ($gotoes as $goto) {
      if (!\Drupal::service('email.validator')->isValid(trim($goto))
      && $form_state->getValue('goto') !== 'devnull') {
        $form_state->setErrorByName('goto', $this->t('Enter a valid email address to forward.'));
      }
    }

    if (!\Drupal::currentUser()->hasPermission('administer postfix_admin')
    && User::load(\Drupal::currentUser()->id() !== $form_state->getValue('localpart'))) {
      $form_state->setErrorByName('localpart', $this->t('You do not have add/edit the account name.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entity;
    $address = (trim($form_state->getValue('localpart')) !== '*'
             ? trim($form_state->getValue('localpart'))
             : '')
    // Blank if localpart is '*' that means 'catch-all'.
             . '@' . $form_state->getValue('domain');
    // @TODO: unused; Handle multiple email addresses
    $goto = explode('\n', $form_state->getValue('goto'));

    $status = 'error';
    $message = $this->t('@type: The Email Address "@label" failed to save.', [
      '@type'  => $entity->getEntityType()->getLabel(),
      '@label' => $entity->label(),
    ]);

    $entity->setAddress($address);
    $entity->setGoto($goto[0]);
    $entity->setActive((bool) $form_state->getValue('active'));

    if (!$entity->isActivated()
    // @TODO: Handle multiple email addresses
    || ($entity->getGoto()[0] !== trim($form_state->getValue('goto')))) {

      $entity->setAddress($address);
      $entity->setDomain($form_state->getValue('domain'));
      $entity->setGoto(trim($form_state->getValue('goto')));

      \Drupal::currentUser()->hasPermission('administer postfix_admin')
      ? $entity->setActive(TRUE)
      : $entity->setActive(FALSE);

      $entity->setActivated(FALSE);
      \Drupal::config('postfix_admin.settings')->get('email_validation')
      // Send Email to validate the alias.
      ? $entity->sendConfirmationEmail($goto)
      : $entity->setActivated(TRUE);

    }

    $entity->save();
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $message = $this->t('Created the forwarding alias from "%label" to "%goto".', [
          '%label' => $entity->label(),
        // @TODO: Handle multiple email addresses
          '%goto' => $entity->getGoto()[0],
        ]);
        break;

      default:
        $message = $this->t('Saved the forwarding alias from "%label" to "%goto".', [
          '%label' => $entity->label(),
        // @TODO: Handle multiple email addresses
          '%goto' => $entity->getGoto()[0],
        ]);
    }

    $form_state->setRedirect('entity.alias.collection', ['alias' => $entity->id()]);
    \Drupal::service('messenger')->addMessage($message);
  }

}
