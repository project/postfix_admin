<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Domain edit forms.
 *
 * @ingroup postfix_admin
 */
class DomainForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\postfix_admin\Entity\Domain */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $message = $this->t('Created the %label Domain.', [
          '%label' => $entity->label(),
        ]);
        break;

      default:
        $message = $this->t('Saved the %label Domain.', [
          '%label' => $entity->label(),
        ]);
    }

    \Drupal::service('messenger')->addMessage($message);
    $form_state->setRedirect('entity.domain.collection', ['domain' => $entity->id()]);
  }

}
