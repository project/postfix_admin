<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Alias entities.
 *
 * @ingroup postfix_admin
 */
class AliasDeleteForm extends ContentEntityDeleteForm {

}
