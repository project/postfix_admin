<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Mailbox entities.
 *
 * @ingroup postfix_admin
 */
class MailboxDeleteForm extends ContentEntityDeleteForm {


}
