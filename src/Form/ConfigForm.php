<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);
    $config = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profile Name'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#description' => $this->t("Any profile name of this configuration. Only underscore '_' can be accepted as a special character"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => '\Drupal\postfix_admin\Entity\Config::load',
      ],
      '#disabled' => !$config->isNew(),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#maxlength' => 255,
      '#default_value' => $config->username(),
      '#disabled' => FALSE,
      '#description' => $this->t('Postfix Admin - Admin username.'),
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#maxlength' => 255,
      '#default_value' => $config->password(),
      '#disabled' => FALSE,
      '#description' => $this->t('Postfix Admin - Admin password.'),
      '#required' => TRUE,
    ];

    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#maxlength' => 255,
      '#default_value' => $config->isNew() ? 'https://' : $config->hostname(),
      '#disabled' => FALSE,
      '#description' => $this->t("Postfix Admin hostname. Include 'http://' or 'https://' (e.g. https://example.com/postfix_admin); do not include '/xmlrpc.php' at the end of the URL')"),
      '#required' => TRUE,
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $config = $this->entity;
    $status = $config->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Config.', [
          '%label' => $config->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Config.', [
          '%label' => $config->label(),
        ]));
    }
    $form_state->setRedirectUrl($config->toUrl('collection'));
  }

}
