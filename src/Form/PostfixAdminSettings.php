<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AWS Cloud Admin Settings.
 */
class PostfixAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'postfix_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['postfix_admin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = \Drupal::config('postfix_admin.settings');

    $form['email_validation'] = [
      '#type' => 'checkbox',
      '#title' => t('Validate Email address'),
      '#default_value' => $config->get('email_validation'),
      '#description' => t('Check if you want to verify an alias email address by sending a confirmation email.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('postfix_admin.settings');
    $form_state->cleanValues();

    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, Html::escape($value));
    }
    $config->save();

    \Drupal::service('messenger')->addMessage(t('Saved the Postfix Admin settings.'));
    $form_state->setRedirect('system.admin_config');
  }

}
