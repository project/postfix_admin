<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\postfix_admin\Entity;

/**
 * Form controller for Alias Domain edit forms.
 *
 * @ingroup postfix_admin
 */
class AliasDomainForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /* @var $entity \Drupal\postfix_admin\Entity\AliasDomain */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    // The domain name.
    $options = [];
    foreach (Domain::getDomains() as $domain) {
      $options['alias_domain'][$domain] = $domain;
      $options['target_domain'][$domain] = $domain;
    }

    if (empty($options['target_domain'])) {
      \Drupal::service('messenger')->addMessage(
        $this->t('Please add domain names at first.'), 'warning', FALSE
      );
      $response = new RedirectResponse(\Drupal::url('entity.domain.add_form'));
      $response->send();
    }

    $entity_query = \Drupal::service('entity.query')->get('alias_domain');
    $entity_query->condition('target_domain', 0, '<>');
    $query_result = $entity_query->execute();
    $alias_domains = \Drupal::entityTypeManager()
      ->getStorage('alias_domain')
      ->loadMultiple($query_result);
    foreach ($alias_domains as $alias_domain) {
      if ($entity->getTargetDomain() !== $alias_domain->getTargetDomain()) {
        // Delete the existing registered target_domain from alias_domain list
        // for select options.
        unset($options['target_domain'][$alias_domain->getTargetDomain()]);
      }
    }

    $form['alias_domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Alias Domain'),
      '#options' => $options['alias_domain'],
      '#default_value' => $entity->getAliasDomain(),
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('Select an alias domain name'),
      '#weight' => -10,
      '#required' => TRUE,
    ];

    $form['target_domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Target Domain'),
      '#options' => $options['target_domain'],
      '#default_value' => $entity->getTargetDomain(),
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('Select a target domain name'),
      '#weight' => -8,
      '#required' => TRUE,
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => (bool) $entity->isActive(),
      '#description' => $this->t('Check to enable the Domain.'),
      '#attributes'  => ['readonly' => 'readonly'],
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->entity;
    if ($form_state->getValue('alias_domain') === $form_state->getValue('target_domain')) {
      $form_state->setErrorByName('target_domain',
        $this->t('A domain "@garget_domain" cannot be an alias domain to itself.', [
          $form_state->getValue('target_domain'),
        ])
      );
    }

    $entity_query = \Drupal::service('entity.query')->get('alias_domain');
    $entity_query->condition('alias_domain', 0, '<>');
    $query_result = $entity_query->execute();
    $alias_domains = \Drupal::entityTypeManager()->getStorage('alias_domain')->loadMultiple($query_result);
    foreach ($alias_domains as $alias_domain) {
      if ($form_state->getValue('alias_domain') === $alias_domain->getAliasDomain()) {
        // Adding newly.
        if ($entity->isNew()) {
          $form_state->setErrorByName('alias_domain',
            $this->t('An alias domain "@alias_domain" is already associated with a target domain "@target_domain".', [
              '@alias_domain' => $form_state->getValue('alias_domain'),
              '@target_domain' => $alias_domain->getTargetDomain(),
            ])
          );
        }
        // Edit mode.
        elseif ($form_state->getValue('target_domain') !== $alias_domain->getTargetDomain()) {
          $form_state->setErrorByName('target_domain',
            $this->t('An alias domain "@alias_domain" is already associated with a target domain "@target_domain".', [
              '@alias_domain' => $form_state->getValue('alias_domain'),
              '@target_domain' => $alias_domain->getTargetDomain(),
            ])
          );
        }
      }
    }
  }

  /**
   * Don't use save() method when you added validateForm.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entity;
    $entity->setAliasDomain($form_state->getValue('alias_domain'));
    $entity->setTargetDomain($form_state->getValue('target_domain'));
    $entity->setActive($form_state->getValue('active'));
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $message = $this->t('Created the %label Alias Domain.', [
          '%label' => $entity->label(),
        ]);
        break;

      default:
        $message = $this->t('Saved the %label Alias Domain.', [
          '%label' => $entity->label(),
        ]);
    }

    \Drupal::service('messenger')->addMessage($message);
    $form_state->setRedirect('entity.alias_domain.collection', ['alias_domain' => $entity->id()]);
  }

}
