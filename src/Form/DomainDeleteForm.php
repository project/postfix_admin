<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Domain entities.
 *
 * @ingroup postfix_admin
 */
class DomainDeleteForm extends ContentEntityDeleteForm {


}
