<?php

namespace Drupal\postfix_admin\Form;

use Drupal\user\Entity\User;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity;
use Drupal\postfix_admin\Entity;

// Use Symfony\Component\HttpFoundation\RedirectResponse;.
/**
 * Form controller for Mailbox edit forms.
 *
 * @ingroup postfix_admin
 */
class MailboxForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\postfix_admin\Entity\Mailbox */

    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $options = [];
    foreach (Domain::getDomains() as $domain) {
      $options[$domain] = $domain;
    }

    // The account name before '@' in email address.
    $account = User::load(\Drupal::currentUser()->id());
    $form['local_part'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account Name'),
      '#maxlength' => 255,
      '#default_value' => $account->getUsername(),
      '#description' =>
      ''
      '#weight' => -50,
      '#disabled' => TRUE,
      '#required' => TRUE,
    ];

    $entity_query = \Drupal::service('entity.query')->get('mailbox');
    $entity_query->condition('username', 0, '<>');
    $query_result = $entity_query->execute();
    $mailboxes = \Drupal::entityTypeManager()->getStorage('mailbox')->loadMultiple($query_result);
    foreach ($mailboxes as $mailbox) {
      if ($entity->getUsername() !== $mailbox->getUsername()) {
        // Delete the existing registered target_domain from alias_domain list
        // for select options.
        unset($options[$mailbox->getDomain()]);
      }
    }

    if (empty($options)) {
      \Drupal::service('messenger')->addMessage($this->t('Please add domain names at first.'), 'warning', FALSE);
      $response = new RedirectResponse(\Drupal::url('entity.domain.add_form'));
      $response->send();
    }

    $form['domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Domain'),
      '#options' => $options,
      '#default_value' => $entity->getDomain(),
      '#attributes' => ['readonly' => 'readonly'],
      '#description' => $this->t('Select a domain name'),
      '#weight' => -48,
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#maxlength' => 255,
      '#default_value' => $entity->getPassword(),
      '#description' => $this->t('The password of the Mailbox user.'),
      '#weight' => -46,
      '#disabled' => FALSE,
      '#required' => TRUE,
    ];

    $form['password_confirmation'] = [
      '#type' => 'password',
      '#title' => $this->t('Confirm Password'),
      '#maxlength' => 255,
      '#default_value' => $entity->getPasswordConfirmation(),
      '#description' => $this->t('The password confirmation of the Mailbox user.'),
      '#weight' => -44,
      '#disabled' => FALSE,
      '#required' => TRUE,
    ];

    $form['quota'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quota'),
      '#maxlength' => 255,
      '#default_value' => $entity->getQuota() ? round($entity->getQuota() / 1024 / 1000) : 10,
      '#description' => $this->t('MB (max: 10).'),
      '#attributes' => [
        ' type' => 'number',
        'min' => 1,
        'max' => 10,
        'step' => 1,
      ],
      '#weight' => -1,
      '#disabled' => FALSE,
      '#required' => TRUE,
    ];

    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone'),
      '#maxlength' => 32,
      '#default_value' => $entity->getPhone(),
      '#description' => $this->t('The phone number of the Mailbox user.'),
      '#attributes' => [
        ' type' => ' tel',
      ],
      '#weight' => -1,
      '#disabled' => FALSE,
      '#required' => FALSE,
    ];

    $form['email_other'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Other Email'),
      '#maxlength' => 255,
      '#default_value' => $entity->getEmailOther(),
      '#description' => $this->t('The other Email address of the Mailbox user.'),
      '#attributes' => [
        ' type' => ' email',
      ],
      '#weight' => -1,
      '#disabled' => FALSE,
      '#required' => FALSE,
    ];

    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => $entity->isActive(),
      '#description' => $this->t('Check to enable the Mailbox.'),
      '#attributes'  => ['readonly' => 'readonly'],
      '#weight' => 50,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entity;

    // Check if the requested Email address already exists in mailbox or not.
    $entity_query = $this->query_factory->get('mailbox');
    $entity_query->condition('username', 0, '<>');
    $query_result = $entity_query->execute();
    $mailboxes = \Drupal::entityTypeManager()->getStorage('mailbox')->loadMultiple($query_result);
    foreach ($mailboxes as $mailbox) {
      // Adding a new one.
      if ($entity->isNew()
      && $mailbox->getUsername() === $form_state->getValue('local_part') . '@' . $form_state->getValue('domain')) {
        $form_state->setErrorByName('domain',
          $this->t('The Email address "'@mailbox'" already exists.  Please choose a different one.', [
            '@mailbox' => $mailbox->getUsername(),
          ]);
        );
      }
    }

    // Check if the requested Email address already exists in mailbox or not.
    $entity_query = $this->query_factory->get('alias');
    $entity_query->condition('address', 0, '<>');
    $query_result = $entity_query->execute();
    $aliases = \Drupal::entityTypeManager()->getStorage('alias')->loadMultiple($query_result);
    foreach ($aliases as $alias) {
      if ($alias->getAddress() === $form_state->getValue('local_part') . '@' . $form_state->getValue('domain')) {
        $form_state->setErrorByName('domain',
          $this->t('The Email address "'@alias'" already exists.  Please choose a different one.', [
            '@alias' => $alias->getAddress(),
          ])
        );
      }
    }

    if ($form_state->getValue('quota') > 10) {
      $form_state->setErrorByName('quota',
        $this->t('The quota that you specified is too high ('@quota' MB)', [
          '@quota' => $form_state->getValue('quota'),
        ])
      );
    }

    if (!ctype_digit(strval($form_state->getValue('quota')))) {
      $form_state->setErrorByName('quota', $this->t('The quota is not allowed to be other than positive integer'));
    }

    if ($form_state->getValue('password') !== $form_state->getValue('password_confirmation')) {
      $form_state->setErrorByName('password', $this->t("The passwords that you supplied don't match."));
    }

    if (!empty($form_state->getValue('email_other'))
      && !\Drupal::service('email.validator')->isValid($form_state->getValue('email_other'))) {
      $form_state->setErrorByName('email_other', $this->t('Enter a valid email address for Email Other.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity = $this->entity;
    $entity->setUsername($form_state->getValue('local_part') . '@' . $form_state->getValue('domain'));
    $entity->setLocalPart($form_state->getValue('local_part'));
    $entity->setName($form_state->getValue('name'));
    $entity->setDomain($form_state->getValue('domain'));
    $entity->setPassword(password_hash($form_state->getValue('password'), PASSWORD_DEFAULT));
    $entity->setPasswordConfirmation(password_hash($form_state->getValue('password_confirmation'), PASSWORD_DEFAULT));
    $entity->setMailDir($form_state->getValue('maildir') ?: '/' . $form_state->getValue('domain') . '/' . $form_state->getValue('local_part'));
    $entity->setQuota($form_state->getValue('quota') * 1024 * 1000);
    $entity->setPhone($form_state->getValue('phone'));
    $entity->setActive($form_state->getValue('active'));
    $entity->save();
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $message = $this->t('Created the %label Mailbox.', [
          '%label' => $entity->label(),
        ]);
        break;

      default:
        $message = $this->t('Saved the %label Mailbox.', [
          '%label' => $entity->label(),
        ]);
    }

    \Drupal::service('messenger')->addMessage($message);
    $form_state->setRedirect('entity.mailbox.collection', ['mailbox' => $entity->id()]);
  }

}
