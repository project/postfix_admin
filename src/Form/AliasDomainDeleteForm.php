<?php

namespace Drupal\postfix_admin\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Alias Domain entities.
 *
 * @ingroup postfix_admin
 */
class AliasDomainDeleteForm extends ContentEntityDeleteForm {


}
