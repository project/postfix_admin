<?php

/**
 * @file
 * Contains alias_domain.page.inc.
 *
 * Page callback for Alias Domain entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Alias Domain templates.
 *
 * Default template: alias_domain.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_alias_domain(array &$variables) {
  // Fetch AliasDomain Entity Object.
  $alias_domain = $variables['elements']['#alias_domain'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
