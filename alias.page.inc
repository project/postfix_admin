<?php

/**
 * @file
 * Contains alias.page.inc.
 *
 * Page callback for Alias entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Alias templates.
 *
 * Default template: alias.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_alias(array &$variables) {
  // Fetch alias Entity Object.
  $alias = $variables['elements']['#alias'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
