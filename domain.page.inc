<?php

/**
 * @file
 * Contains domain.page.inc.
 *
 * Page callback for Domain entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Domain templates.
 *
 * Default template: domain.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_domain(array &$variables) {
  // Fetch Domain Entity Object.
  $domain = $variables['elements']['#domain'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
