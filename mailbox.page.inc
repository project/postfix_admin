<?php

/**
 * @file
 * Contains mailbox.page.inc.
 *
 * Page callback for Mailbox entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Mailbox templates.
 *
 * Default template: mailbox.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_mailbox(array &$variables) {
  // Fetch Mailbox Entity Object.
  $mailbox = $variables['elements']['#mailbox'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
